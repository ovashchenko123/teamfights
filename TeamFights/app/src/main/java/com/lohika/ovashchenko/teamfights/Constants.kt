package com.lohika.ovashchenko.teamfights

object Constants {
    object DatabaseCollections {
        const val TEAMS_COLLECTION = "teams"
        const val USER_TEAMS_COLLECTION = "userTeams"
        const val TEAMS_LOGO_FOLDER = "team_logos"
    }

    const val ONE_MEGABYTE = (1024 * 1024).toLong()
}