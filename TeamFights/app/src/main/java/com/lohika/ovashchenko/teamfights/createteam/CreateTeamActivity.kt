package com.lohika.ovashchenko.teamfights.createteam

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.lohika.ovashchenko.teamfights.ErrorHandler
import com.lohika.ovashchenko.teamfights.R
import com.lohika.ovashchenko.teamfights.main.MainActivity
import com.lohika.ovashchenko.teamfights.model.Result
import com.lohika.ovashchenko.teamfights.utils.observe
import kotlinx.android.synthetic.main.activity_create_team.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class CreateTeamActivity : AppCompatActivity() {
    private val createTeamViewModel by viewModel<CreateTeamViewModel>()
    private var logoUri: Uri? = null

    companion object {
        const val PICK_IMAGE = 250
        fun start(context: Context) {
            val intent = Intent(context, CreateTeamActivity::class.java)
            context.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_team)
        cancel.setOnClickListener { onBackPressed() }
        createTeamViewModel.liveCreateTeam.observe(this) { handleTeamCreation(it) }
        save.setOnClickListener { validateAndSave() }

        upload_logo.setOnClickListener {
            val intent = Intent()
            intent.type = "image/*"
            intent.action = Intent.ACTION_GET_CONTENT
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == PICK_IMAGE && resultCode == Activity.RESULT_OK) {
            logoUri = data?.data
            team_logo.setImageURI(logoUri)
        }
    }

  private fun handleTeamCreation(it: Result<Unit>) {
    when (it.status) {
      Result.Status.SUCCESS -> {
        MainActivity.startNewTask(this)
      }
      Result.Status.ERROR -> {
        ErrorHandler.showError(this, it.errorMessage)
      }
      Result.Status.LOADING -> {
        //TODO: Show spinner
      }
    }
  }

    private fun validateAndSave() {
        val teamName = team_name.text.toString()
        val teamSport = team_sport.selectedItem.toString()
        if (logoUri == null) {
            ErrorHandler.showError(this, "Please select logo")
            return
        }
        if (teamName.isEmpty() || teamSport.isEmpty()) {
            ErrorHandler.showError(this, "Please fill all fields")
            return
        }

        createTeamViewModel.upload(
                teamName,
                teamSport,
                contentResolver.openInputStream(logoUri))
    }
}
