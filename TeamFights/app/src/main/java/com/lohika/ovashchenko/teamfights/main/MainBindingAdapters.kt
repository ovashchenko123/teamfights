package com.lohika.ovashchenko.teamfights.main

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.squareup.picasso.Picasso

@BindingAdapter("url", "placeholder", requireAll = true)
fun loadImage(imageView: ImageView, url: String?, placeholder: Int) {
    Picasso.get()
            .load(url)
            .placeholder(placeholder)
            .into(imageView)
}