package com.lohika.ovashchenko.teamfights.main

import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.ImageView
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.core.view.get
import androidx.databinding.DataBindingUtil
import com.google.android.material.navigation.NavigationView
import com.lohika.ovashchenko.teamfights.ErrorHandler
import com.lohika.ovashchenko.teamfights.R
import com.lohika.ovashchenko.teamfights.chat.ChatActivity
import com.lohika.ovashchenko.teamfights.createteam.CreateTeamActivity
import com.lohika.ovashchenko.teamfights.databinding.NavHeaderMainBinding
import com.lohika.ovashchenko.teamfights.model.Result
import com.lohika.ovashchenko.teamfights.model.Team
import com.lohika.ovashchenko.teamfights.profile.ProfileActivity
import com.lohika.ovashchenko.teamfights.settings.SettingsActivity
import com.lohika.ovashchenko.teamfights.signin.SignInActivity
import com.lohika.ovashchenko.teamfights.utils.addFragment
import com.lohika.ovashchenko.teamfights.utils.observe
import com.lohika.ovashchenko.teamfights.utils.replaceFragment
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import org.koin.androidx.viewmodel.ext.android.viewModel


class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {
    companion object {
        fun start(context: Context) {
            context.startActivity(Intent(context, MainActivity::class.java))
        }

        fun startNewTask(context: Context) {
            Intent(context, MainActivity::class.java).apply {
                addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                context.startActivity(this)
            }
        }
    }

    private val mainViewModel by viewModel<MainViewModel>()
    private val myTeams = ArrayList<Team>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        setDrawerToggle()
        setupNavigation()
        setupBottomNavigation()
        mainViewModel.liveMyTeams.observe(this) { handleTeamsReceiving(it) }
        mainViewModel.getMyTeams()
        // No teams by default

        navView.menu[0].isVisible = false

        // TODO: update ui for this
        addFragment(R.id.fragment_container, NoTeamFragment.newInstance(), NoTeamFragment.tag())
    }

    override fun onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.chat, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_chat -> {
                // TODO: need to provide real data for chat screen
                val currentTeamName = "My Best Team"
                val currentTeamId = "PstevPmQ7TcRtyZhJgBS"
                ChatActivity.start(this, currentTeamName, currentTeamId)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_add_team -> CreateTeamActivity.start(this)
            R.id.nav_settings -> SettingsActivity.start(this)
            R.id.nav_profile -> ProfileActivity.start(this)
            R.id.nav_sign_out -> processSignOut()
        }

        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    private fun handleTeamsReceiving(result: Result<Team>) {
        if (result.status == Result.Status.SUCCESS) {
            // found team
            navView.menu[0].isVisible = true
            val menuItem = navView.menu[0].subMenu.add("")
            mainViewModel.getTeamLogo(result.data).observe(this) {
                handleLogoReceiving(it, menuItem)
            }
            myTeams.add(result.data)
        } else {
            ErrorHandler.showError(this, result.errorMessage)
        }
    }

    private fun handleLogoReceiving(result: Result<ByteArray>, menuItem: MenuItem) {
        if (result.status == Result.Status.SUCCESS) {
            val image = BitmapDrawable(resources, BitmapFactory.decodeByteArray(result.data, 0, result.data.size))
            val imageView = ImageView(this)
            imageView.maxHeight = 10
            imageView.setPadding(0, 10, 0, 10)
            imageView.setImageDrawable(image)
            menuItem.actionView = imageView
        }
        if (result.status == Result.Status.ERROR) {
            ErrorHandler.showError(this, result.errorMessage)
        }
    }

    private fun setupBottomNavigation() {
        bottom_navigation.setOnNavigationItemSelectedListener {
            return@setOnNavigationItemSelectedListener when (it.itemId) {
                R.id.action_requests -> {
                    replaceFragment(R.id.fragment_container, RequestsFragment.newInstance(), RequestsFragment.tag())
                    true
                }
                R.id.action_trainings -> {
                    replaceFragment(R.id.fragment_container, TrainingsFragment.newInstance(), TrainingsFragment.tag())
                    true
                }
                R.id.action_team -> {
                    replaceFragment(R.id.fragment_container, TeamFragment.newInstance(), TeamFragment.tag())
                    true
                }
                R.id.action_statistic -> {
                    replaceFragment(R.id.fragment_container, StatisticFragment.newInstance(), StatisticFragment.tag())
                    true
                }
                else -> false
            }
        }
    }

    private fun setupNavigation() {
        navView.setNavigationItemSelectedListener(this)
        DataBindingUtil.bind<NavHeaderMainBinding>(navView.getHeaderView(0))?.apply {
            setLifecycleOwner(this@MainActivity)
            model = mainViewModel
        }
    }

    private fun setDrawerToggle() {
        ActionBarDrawerToggle(
                this,
                drawerLayout,
                toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close
        ).apply {
            drawerLayout.addDrawerListener(this)
            syncState()
        }
    }

    private fun processSignOut() = mainViewModel.signOut().observe(this) {
        SignInActivity.start(this)
    }
}
