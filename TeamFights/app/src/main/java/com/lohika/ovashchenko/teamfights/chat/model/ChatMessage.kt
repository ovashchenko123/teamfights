package com.lohika.ovashchenko.teamfights.chat.model

import com.google.firebase.firestore.Exclude
import com.google.firebase.firestore.ServerTimestamp
import java.util.*

class ChatMessage(var sender: String = "",
                  var senderId: String = "",
                  var senderUrl: String? = null,
                  var message: String = ""
) : Comparable<ChatMessage> {
    @ServerTimestamp
    var timestamp: Date? = null
    @Exclude
    var id: String? = null
    @Exclude
    var status: String? = null

    override fun compareTo(other: ChatMessage): Int {
        return this.timestamp?.compareTo(other.timestamp) ?: if (other.timestamp == null) 0 else -1
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ChatMessage

        if (sender != other.sender) return false
        if (senderId != other.senderId) return false
        if (senderUrl != other.senderUrl) return false
        if (message != other.message) return false
        if (timestamp != other.timestamp) return false
        if (id != other.id) return false
        if (status != other.status) return false

        return true
    }

    override fun hashCode(): Int {
        var result = sender.hashCode()
        result = 31 * result + senderId.hashCode()
        result = 31 * result + (senderUrl?.hashCode() ?: 0)
        result = 31 * result + message.hashCode()
        result = 31 * result + (timestamp?.hashCode() ?: 0)
        result = 31 * result + (id?.hashCode() ?: 0)
        result = 31 * result + (status?.hashCode() ?: 0)
        return result
    }
}