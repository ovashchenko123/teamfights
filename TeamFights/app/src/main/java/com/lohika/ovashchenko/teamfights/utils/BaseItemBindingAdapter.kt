package com.lohika.ovashchenko.teamfights.utils

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import java.util.*

abstract class BaseItemBindingAdapter<T> : RecyclerView.Adapter<ItemBindingViewHolder>() {

    var content = ArrayList<T>()

    val empty: Boolean
        get() = content.isEmpty()

    protected abstract fun createItemViewBinding(
            inflater: LayoutInflater,
            parent: ViewGroup,
            viewType: Int
    ): ViewDataBinding

    protected abstract fun bindItemViewHolder(binding: ViewDataBinding, item: T)

    protected fun getItem(position: Int): T {
        return content[position]
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemBindingViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ItemBindingViewHolder(createItemViewBinding(inflater, parent, viewType))
    }

    override fun onBindViewHolder(holder: ItemBindingViewHolder, position: Int) {
        bindItemViewHolder(holder.binding, getItem(position))
    }

    override fun getItemCount(): Int {
        return content.size
    }
}