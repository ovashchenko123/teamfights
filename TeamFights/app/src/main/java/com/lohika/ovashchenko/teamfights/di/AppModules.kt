package com.lohika.ovashchenko.teamfights.di

import android.app.Application
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.lohika.ovashchenko.teamfights.R
import com.lohika.ovashchenko.teamfights.chat.ChatViewModel
import com.lohika.ovashchenko.teamfights.createteam.CreateTeamViewModel
import com.lohika.ovashchenko.teamfights.main.MainViewModel
import com.lohika.ovashchenko.teamfights.signin.SignInViewModel
import com.lohika.ovashchenko.teamfights.utils.ConnectivityLiveData
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.Module
import org.koin.dsl.module.module

// Koin module
val firebaseModule: Module = module {
    single { FirebaseAuth.getInstance() }
    single { FirebaseFirestore.getInstance() }
    single { FirebaseStorage.getInstance() }
}

val googleSignInModule: Module = module {
    single {
        GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(androidContext().getString(R.string.default_web_client_id))
                .requestEmail()
                .build()
    }
    single { GoogleSignIn.getClient(androidContext(), get()) }
}

val viewModelsModule: Module = module {
    single { ConnectivityLiveData(androidContext().applicationContext as Application) }
    viewModel { SignInViewModel(get(), get()) }
    viewModel { CreateTeamViewModel(get(), get(), get()) }
    viewModel { MainViewModel(get(), get(), get(), get()) }
    viewModel { (teamId: String) -> ChatViewModel(get(), get(), get(), teamId) }
}