package com.lohika.ovashchenko.teamfights.createteam

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.lohika.ovashchenko.teamfights.Constants
import com.lohika.ovashchenko.teamfights.model.Result
import com.lohika.ovashchenko.teamfights.model.Team
import com.lohika.ovashchenko.teamfights.model.UserTeams
import java.io.InputStream
import java.util.*

class CreateTeamViewModel(private val store: FirebaseFirestore,
        private val firebaseAuth: FirebaseAuth,
        storage: FirebaseStorage) : ViewModel() {

    private val storageLogoRef = storage.reference.child(Constants.DatabaseCollections.TEAMS_LOGO_FOLDER)

    val liveCreateTeam = MutableLiveData<Result<Unit>>()

    //TODO: push it to repository
    fun upload(teamName: String, teamSport: String, logoStream: InputStream) {
        // TODO: use coroutines in future
        val uploadTask = storageLogoRef.child(teamName + UUID.randomUUID().toString()).putStream(logoStream)
        // Uploading team logo
        uploadTask.addOnSuccessListener { snapshot ->
            store.collection(Constants.DatabaseCollections.TEAMS_COLLECTION) // Creating team
                    .add(Team(teamName,
                            teamSport,
                            firebaseAuth.uid!!,
                            store.document(snapshot.storage.path)))
                    .addOnSuccessListener { teamDocumentReference ->
                        // Adding team owner to the team
                        store.collection(Constants.DatabaseCollections.USER_TEAMS_COLLECTION)
                                .document(firebaseAuth.uid!!)
                                .get()
                                .addOnSuccessListener { documentSnapshot ->
                                    var userTeams = documentSnapshot.toObject(UserTeams::class.java)
                                    if (userTeams == null) {
                                        userTeams = UserTeams(mutableListOf(teamDocumentReference.id))
                                    } else {
                                        userTeams.teamIds.add(teamDocumentReference.id)
                                    }
                                    store.collection(Constants.DatabaseCollections.USER_TEAMS_COLLECTION)
                                            .document(firebaseAuth.uid!!)
                                            .set(userTeams)
                                            .addOnSuccessListener { liveCreateTeam.value = Result.success(Unit) }
                                            .addOnFailureListener {
                                                liveCreateTeam.value = Result.error(Unit, it.localizedMessage)
                                            }
                                }

                    }
                    .addOnFailureListener {
                        liveCreateTeam.value = Result.error(Unit, it.localizedMessage)
                    }
        }
        uploadTask.addOnFailureListener { error ->
            liveCreateTeam.value = Result.error(Unit, error.localizedMessage)
        }
    }

}