package com.lohika.ovashchenko.teamfights.model

import com.google.firebase.firestore.ServerTimestamp
import java.util.*

class UserTeams(val teamIds: MutableList<String> = ArrayList()) {
    @ServerTimestamp
    var createdDate: Date? = null
}