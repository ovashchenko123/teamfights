package com.lohika.ovashchenko.teamfights.utils

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment


fun AppCompatActivity.addFragment(containerId: Int,
                                  fragment: Fragment,
                                  fragmentTag: String
): Int {
        return supportFragmentManager
                .beginTransaction()
                .add(containerId, fragment, fragmentTag)
                .disallowAddToBackStack()
                .commit()
}

fun AppCompatActivity.replaceFragment(containerId: Int,
                                  fragment: Fragment,
                                  fragmentTag: String
): Int {
        return supportFragmentManager
                .beginTransaction()
                .replace(containerId, fragment, fragmentTag)
                .commit()
}