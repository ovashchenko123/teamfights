package com.lohika.ovashchenko.teamfights.chat

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.lohika.ovashchenko.teamfights.R
import com.lohika.ovashchenko.teamfights.databinding.ActivityChatBinding
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class ChatActivity : AppCompatActivity() {
    companion object {

        private const val EXTRA_TEAM_NAME = "EXTRA_TEAM_NAME"
        private const val EXTRA_TEAM_ID = "EXTRA_TEAM_ID"

        fun start(context: Context, teamName: String, teamId: String) {
            val intent = Intent(context, ChatActivity::class.java)
            intent.putExtra(EXTRA_TEAM_NAME, teamName)
            intent.putExtra(EXTRA_TEAM_ID, teamId)
            context.startActivity(intent)
        }
    }

    private val chatViewModel: ChatViewModel by viewModel { parametersOf(intent.getStringExtra(EXTRA_TEAM_ID)) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        title = getString(R.string.team_chat_title, intent?.getStringExtra(EXTRA_TEAM_NAME))
        DataBindingUtil.setContentView<ActivityChatBinding>(this, R.layout.activity_chat).apply {
            setLifecycleOwner(this@ChatActivity)
            chatAdapter = ChatAdapter(chatViewModel.getCurrentUserId())
            model = chatViewModel
        }
        chatViewModel.load()
    }
}
