package com.lohika.ovashchenko.teamfights

import android.app.Application
import com.lohika.ovashchenko.teamfights.di.firebaseModule
import com.lohika.ovashchenko.teamfights.di.googleSignInModule
import com.lohika.ovashchenko.teamfights.di.viewModelsModule
import org.koin.android.ext.android.startKoin
import org.koin.android.logger.AndroidLogger
import org.koin.log.EmptyLogger

class TFApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        // Start Koin
        startKoin(this,
                listOf(firebaseModule, googleSignInModule, viewModelsModule),
                // Disable logs of KOIN in release.
                logger = if (BuildConfig.DEBUG) AndroidLogger() else EmptyLogger()
        )
    }
}