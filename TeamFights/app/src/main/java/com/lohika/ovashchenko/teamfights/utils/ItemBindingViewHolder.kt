package com.lohika.ovashchenko.teamfights.utils

import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView

class ItemBindingViewHolder(val binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root)