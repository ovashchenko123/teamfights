package com.lohika.ovashchenko.teamfights

import android.content.Context
import android.widget.Toast

class ErrorHandler {
    companion object {
        fun showError(context: Context, text: String) {
            //TODO: create some UI for this
            Toast.makeText(context, text, Toast.LENGTH_SHORT).show()
        }
    }
}