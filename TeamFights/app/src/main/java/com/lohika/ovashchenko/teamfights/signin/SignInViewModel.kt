package com.lohika.ovashchenko.teamfights.signin

import android.content.Intent
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.GoogleAuthProvider

class SignInViewModel(private val firebaseAuth: FirebaseAuth,
                      private val googleClient: GoogleSignInClient
) : ViewModel() {
    val isSigningIn = MutableLiveData<Boolean>().apply { value = false }
    val liveSignInIntent = MutableLiveData<Intent>()
    val liveCurrentUser = MutableLiveData<FirebaseUser>()
    val liveAuthResult = MutableLiveData<UserAuthResult>()

    fun signIn() {
        showProgress()
        liveSignInIntent.value = googleClient.signInIntent
    }

    fun start() {
        firebaseAuth.currentUser?.let {
            liveCurrentUser.value = it
        }
    }

    fun isAuthenticated(): Boolean = firebaseAuth.currentUser != null

    fun handleSignInResult(task: Task<GoogleSignInAccount>) {
        try {
            val account = task.getResult(ApiException::class.java)
            authWithGoogle(account)
        } catch (e: ApiException) {
            liveAuthResult.value = UserAuthResult(false, false, e)
            hideProgress()
        }
    }

    private fun authWithGoogle(account: GoogleSignInAccount) {
        val credential = GoogleAuthProvider.getCredential(account.idToken, null)
        firebaseAuth.signInWithCredential(credential).addOnCompleteListener {
            if (it.isSuccessful) {
                liveAuthResult.value = UserAuthResult(true, true, null)
            } else {
                liveAuthResult.value = UserAuthResult(true, false, it.exception)
            }
            hideProgress()
        }
    }

    private fun hideProgress() {
        isSigningIn.value = false
    }

    private fun showProgress() {
        isSigningIn.value = true
    }
}

data class UserAuthResult(val hasAccount: Boolean, val isAuthenticated: Boolean, val error: Exception?)
