package com.lohika.ovashchenko.teamfights.chat

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import com.lohika.ovashchenko.teamfights.BR
import com.lohika.ovashchenko.teamfights.chat.model.ChatMessage
import com.lohika.ovashchenko.teamfights.databinding.ChatItemMyMessageBinding
import com.lohika.ovashchenko.teamfights.databinding.ChatItemOthersMessageBinding
import com.lohika.ovashchenko.teamfights.utils.BaseItemBindingAdapter

class ChatAdapter(private val userId: String) : BaseItemBindingAdapter<ChatMessage>() {
    companion object {
        const val VIEW_TYPE_MY_MESSAGE = 0
        const val VIEW_TYPE_OTHERS_MESSAGE = 1
    }

    private val idToMessageIndexMap = hashMapOf<String, Int>()

    override fun getItemViewType(position: Int): Int = when (userId) {
        getItem(position).senderId -> VIEW_TYPE_MY_MESSAGE
        else -> VIEW_TYPE_OTHERS_MESSAGE
    }

    override fun createItemViewBinding(inflater: LayoutInflater, parent: ViewGroup, viewType: Int): ViewDataBinding =
            when (viewType) {
                VIEW_TYPE_MY_MESSAGE -> ChatItemMyMessageBinding.inflate(inflater, parent, false)
                VIEW_TYPE_OTHERS_MESSAGE -> ChatItemOthersMessageBinding.inflate(inflater, parent, false)
                else -> throw UnsupportedOperationException("Unsupported view type")
            }

    override fun bindItemViewHolder(binding: ViewDataBinding, item: ChatMessage) {
        binding.setVariable(BR.chatMessage, item)
    }

    fun addContent(values: List<ChatMessage>): Boolean {
        val updateList = mutableListOf<Int>()
        val addList = mutableListOf<Int>()
        values.forEach {
            var index = idToMessageIndexMap[it.id]
            if (index == null) {
                index = content.size
                content.add(index, it)
                idToMessageIndexMap[it.id!!] = index
                addList.add(index)
            } else if (it != content[index]) {
                content[index] = it
                updateList.add(index)
            }
        }
        if (updateList.isNotEmpty()) {
            updateList.forEach { notifyItemChanged(it) }
        }
        if (addList.isNotEmpty()) {
            addList.forEach { notifyItemInserted(it) }
            return true
        }
        return false
    }
}