package com.lohika.ovashchenko.teamfights.utils

import android.app.Application
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkInfo
import android.os.Build
import android.os.Handler
import android.os.Looper
import androidx.annotation.RequiresApi
import androidx.core.net.ConnectivityManagerCompat
import androidx.lifecycle.LiveData

class ConnectivityLiveData(private val application: Application) : LiveData<Boolean>() {

    private val manager = application.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    private var broadcastReceiver: BroadcastReceiver? = null
    private var networkCallback: ConnectivityManager.NetworkCallback? = null

    override fun onActive() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            registerCallback()
        } else {
            registerBroadcastReceiver()
        }
    }

    override fun onInactive() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            unregisterCallback()
        } else {
            unRegisterBroadcastReceiver()
        }
    }

    @RequiresApi(Build.VERSION_CODES.N)
    private fun registerCallback() {
        if (networkCallback == null) {
            networkCallback = object : ConnectivityManager.NetworkCallback() {
                val handler = Handler(Looper.getMainLooper())

                override fun onLost(network: Network?) {
                    handler.post { value = false }
                }

                override fun onUnavailable() {
                    handler.post { value = false }
                }

                override fun onAvailable(network: Network?) {
                    handler.post { value = true }
                }
            }
            val activeNetworkInfo: NetworkInfo? = manager.activeNetworkInfo
            value = activeNetworkInfo?.isConnected
            manager.registerDefaultNetworkCallback(networkCallback)
        }
    }

    private fun unregisterCallback() {
        if (networkCallback != null) {
            manager.unregisterNetworkCallback(networkCallback)
            networkCallback = null
        }
    }

    @Suppress("DEPRECATION")
    private fun registerBroadcastReceiver() {
        if (broadcastReceiver == null) {
            val filter = IntentFilter()
            filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION)
            broadcastReceiver = object : BroadcastReceiver() {
                override fun onReceive(context: Context, intent: Intent) {
                    val info = ConnectivityManagerCompat.getNetworkInfoFromBroadcast(manager, intent)
                    value = info?.state == NetworkInfo.State.CONNECTED
                }
            }

            application.registerReceiver(broadcastReceiver, filter)
        }
    }

    private fun unRegisterBroadcastReceiver() {
        if (broadcastReceiver != null) {
            application.unregisterReceiver(broadcastReceiver)
            broadcastReceiver = null
        }
    }
}