package com.lohika.ovashchenko.teamfights.chat

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.*
import com.lohika.ovashchenko.teamfights.chat.model.ChatMessage
import com.lohika.ovashchenko.teamfights.utils.ConnectivityLiveData
import java.util.*


class ChatViewModel(firebaseAuth: FirebaseAuth,
                    firestore: FirebaseFirestore,
                    val online: ConnectivityLiveData,
                    teamId: String
) : ViewModel() {
    val contentData = MutableLiveData<List<ChatMessage>>().apply { value = listOf() }
    val contentLoading = MutableLiveData<Boolean>().apply { value = true }
    val messageSent = MutableLiveData<Boolean>().apply { value = false }
    private val currentUser = firebaseAuth.currentUser!!
    private val teamChatCollection: CollectionReference = firestore.collection("teamChats").document(teamId).collection("chat")
    private var registration: ListenerRegistration? = null

    override fun onCleared() {
        registration?.remove()
        super.onCleared()
    }

    fun load() {
        registration = teamChatCollection.addSnapshotListener(MetadataChanges.INCLUDE) { snapshots: QuerySnapshot?, e: FirebaseFirestoreException? ->
            if (e != null) {
                Log.w("MY_TAG", "listen:error", e)
            } else {
                val values: MutableSet<ChatMessage> = TreeSet()
                for (dc in snapshots?.documentChanges!!) {
                    if (dc.type != DocumentChange.Type.REMOVED) {
                        val document = dc.document
                        Log.d("MY_TAG", document.id + " => " + document.data)
                        val chatMessage = document.toObject(ChatMessage::class.java)
                        chatMessage.id = document.id
                        chatMessage.status = if (document.metadata.hasPendingWrites()) "sending" else "sent"
                        values.add(chatMessage)
                    }
                }
                contentData.value = values.toList()
                contentLoading.value = false
            }
        }
    }

    fun sendMessage(message: String) {
        if (message.isEmpty()) {
            return
        }
        messageSent.value = true
        teamChatCollection
                .add(createMessage(message))
                .addOnCompleteListener { task ->
                    Log.d("MY_TAG", "Set DocumentSnapshot result: " + task.result)
                }
    }

    private fun createMessage(message: String): ChatMessage {
        return ChatMessage(
                currentUser.displayName!!,
                currentUser.uid,
                currentUser.photoUrl?.toString(),
                message)
    }

    fun getCurrentUserId(): String {
        return currentUser?.uid
    }
}