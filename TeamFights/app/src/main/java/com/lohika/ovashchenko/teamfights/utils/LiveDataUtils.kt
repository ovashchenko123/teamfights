package com.lohika.ovashchenko.teamfights.utils

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer

inline fun <T> LiveData<T>.observe(owner: LifecycleOwner, crossinline action: (T) -> Unit) =
        observe(owner, Observer<T> { action(it) })