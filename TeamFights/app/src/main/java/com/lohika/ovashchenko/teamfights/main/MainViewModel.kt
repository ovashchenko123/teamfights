package com.lohika.ovashchenko.teamfights.main

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.lohika.ovashchenko.teamfights.Constants
import com.lohika.ovashchenko.teamfights.Constants.ONE_MEGABYTE
import com.lohika.ovashchenko.teamfights.model.Result
import com.lohika.ovashchenko.teamfights.model.Team
import com.lohika.ovashchenko.teamfights.model.UserTeams


class MainViewModel(private val store: FirebaseFirestore,
        private val firebaseAuth: FirebaseAuth,
        private val storage: FirebaseStorage,
        private val googleClient: GoogleSignInClient
) : ViewModel() {

    val userPhotoUrl = MutableLiveData<String>().apply { value = firebaseAuth.currentUser?.photoUrl?.toString() }
    val userName = MutableLiveData<String>().apply { value = firebaseAuth.currentUser?.displayName }
    val userEmail = MutableLiveData<String>().apply { value = firebaseAuth.currentUser?.email }
    val liveMyTeams = MutableLiveData<Result<Team>>()

    private val liveSignOut = MutableLiveData<Unit>()

    //TODO: push it to repository
    fun getMyTeams() {
        val userTeamsReference = store.collection(Constants.DatabaseCollections.USER_TEAMS_COLLECTION).document(
                firebaseAuth.uid!!)
        userTeamsReference.addSnapshotListener { snapshot, e ->
            run {
                if (e != null) {
                    Log.w(MainViewModel::class.java.name, "Listen failed.", e)
                    liveMyTeams.value = Result.error(Team(), e.localizedMessage)
                    return@addSnapshotListener
                }
                if (snapshot != null && snapshot.exists()) {
                    joinTeams(snapshot)
                } else {
                    liveMyTeams.value = Result.success(Team())
                }
            }
        }
    }

    fun getTeamLogo(team: Team) : MutableLiveData<Result<ByteArray>>{
        // Create a storage reference from our app
        val result = MutableLiveData<Result<ByteArray>>()
        val storageRef = storage.reference

        // Create a reference with an initial file path and name
        team.logo?.path?.let { path ->
            val pathReference = storageRef.child(path)

            pathReference.getBytes(ONE_MEGABYTE * 10)
                    .addOnSuccessListener { result.value = Result.success(it) }
                    .addOnFailureListener { result.value = Result.error(ByteArray(0), it.localizedMessage) }
        }

        return result
    }

    private fun joinTeams(snapshot: DocumentSnapshot): Unit? {
        val userTeams = snapshot.toObject(UserTeams::class.java)
        return userTeams?.teamIds?.forEach { teamId ->
            store.collection(Constants.DatabaseCollections.TEAMS_COLLECTION).document(
                    teamId).get().addOnSuccessListener {
                val team = it.toObject(Team::class.java)
                if (team == null) {
                    // something unexpected happened
                    liveMyTeams.value = Result.error(Team(), "Null value")
                } else {
                    liveMyTeams.value = Result.success(team)
                }

            }
        }
    }

    fun signOut(): LiveData<Unit> {
        firebaseAuth.signOut()
        googleClient.signOut().addOnCompleteListener {
            liveSignOut.value = Unit
        }
        return liveSignOut
    }
}