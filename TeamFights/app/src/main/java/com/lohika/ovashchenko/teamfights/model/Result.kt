package com.lohika.ovashchenko.teamfights.model

class Result<E> private constructor(val data: E, val status: Status, val errorMessage: String = "") {
    companion object {
        fun <E> success(data: E) = Result(data, Status.SUCCESS)

        fun <E> loading(data: E) = Result(data, Status.LOADING)

        fun <E> error(data: E, errorMessage: String) = Result(data, Status.ERROR, errorMessage)

    }

    enum class Status {
        SUCCESS, ERROR, LOADING
    }
}