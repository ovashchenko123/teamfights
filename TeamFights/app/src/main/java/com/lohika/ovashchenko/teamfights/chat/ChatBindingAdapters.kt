package com.lohika.ovashchenko.teamfights.chat

import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.lohika.ovashchenko.teamfights.chat.model.ChatMessage
import java.text.SimpleDateFormat
import java.util.*

@BindingAdapter("adapter", "newMessages", requireAll = true)
fun setAdapter(recycler: RecyclerView, adapter: ChatAdapter, data: List<ChatMessage>) {
    if (recycler.adapter == null) {
        recycler.adapter = adapter
    }
    val lastVisibleIndex = (recycler.layoutManager as LinearLayoutManager).findLastCompletelyVisibleItemPosition()
    val isBottomPosition = adapter.itemCount == lastVisibleIndex + 1
    val newItemsAdded = adapter.addContent(data)
    if (isBottomPosition && newItemsAdded) {
        recycler.scrollToPosition(recycler.adapter?.itemCount?.minus(1) ?: 0)
    }
}

@BindingAdapter("timestamp")
fun loadTimestamp(textView: TextView, timestamp: Date?) {
    timestamp?.let {
        textView.text = SimpleDateFormat("HH:mm", Locale.US).format(it)
    }
}

@BindingAdapter("scrollToBottom")
fun scroll(recycler: RecyclerView, scroll: Boolean) {
    if (scroll) {
        recycler.scrollToPosition(recycler.adapter?.itemCount?.minus(1) ?: 0)
    }
}

@BindingAdapter("clearText")
fun clearText(textView: TextView, clear: Boolean?) {
    clear?.let {
        if (it) {
            textView.text = null
        }
    }
}