package com.lohika.ovashchenko.teamfights.model

import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.ServerTimestamp
import java.util.*

class Team(val name: String = "",
        val sport: String = "",
        val captainId: String = "",
        var logo: DocumentReference? = null
) {
    @ServerTimestamp
    var createdDate: Date? = null
}
