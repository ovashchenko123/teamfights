package com.lohika.ovashchenko.teamfights.main

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.lohika.ovashchenko.teamfights.R
import com.lohika.ovashchenko.teamfights.createteam.CreateTeamActivity
import kotlinx.android.synthetic.main.fragment_no_team.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [NoTeamFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [NoTeamFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class NoTeamFragment : Fragment() {

    private var listener: OnFragmentInteractionListener? = null

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        create_team.setOnClickListener { CreateTeamActivity.start(context!!) }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_no_team, container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        }

    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnFragmentInteractionListener {
        //TODO: add some actions
    }

    companion object {
        @JvmStatic
        fun newInstance() = NoTeamFragment()

        fun tag(): String = this::class.java.simpleName
    }
}
