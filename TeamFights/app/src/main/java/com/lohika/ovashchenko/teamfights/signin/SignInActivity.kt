package com.lohika.ovashchenko.teamfights.signin

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.lohika.ovashchenko.teamfights.R
import com.lohika.ovashchenko.teamfights.databinding.ActivityLoginBinding
import com.lohika.ovashchenko.teamfights.main.MainActivity
import com.lohika.ovashchenko.teamfights.utils.observe
import org.koin.androidx.viewmodel.ext.android.viewModel

/**
 * A login screen that offers login via email/password.
 */
class SignInActivity : AppCompatActivity() {

    companion object {

        private const val GOOGLE_SIGN_IN_CODE = 240

        fun start(context: Context) {
            val intent = Intent(context, SignInActivity::class.java)
            intent.apply {
                addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            }
            context.startActivity(intent)
        }
    }

    // Lazy property
    private val signInViewModel by viewModel<SignInViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (signInViewModel.isAuthenticated()) {
            MainActivity.start(this)
            finish()
            return
        }

        DataBindingUtil
                .setContentView<ActivityLoginBinding>(this, R.layout.activity_login)
                .apply {
                    setLifecycleOwner(this@SignInActivity)
                    model = signInViewModel
                }

        signInViewModel.liveSignInIntent.observe(this) {
            startActivityForResult(it, GOOGLE_SIGN_IN_CODE)
        }
        signInViewModel.liveCurrentUser.observe(this) { MainActivity.startNewTask(this) }
        signInViewModel.liveAuthResult.observe(this) {
            when {
                it.hasAccount && it.isAuthenticated -> MainActivity.startNewTask(this)
                !it.isAuthenticated -> Toast.makeText(this, "Authentication Failed.", Toast.LENGTH_LONG).show()
                else -> Toast.makeText(this, "Fail bro.", Toast.LENGTH_LONG).show()
            }
        }
    }

    override fun onStart() {
        super.onStart()
        signInViewModel.start()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == GOOGLE_SIGN_IN_CODE) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            signInViewModel.handleSignInResult(task)
        }
    }
}